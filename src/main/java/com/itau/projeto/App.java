package com.itau.projeto;

import com.github.kevinsawicki.http.HttpRequest;
import com.google.gson.Gson;

public class App 
{
    public static void main( String[] args )
    {
    	Gson gson = new Gson();
    	String response = HttpRequest.get("http://data.fixer.io/api/latest?access_key=578ac533a860ac0f3d508e3dbad0ef57").body();
    	Fixer fixer = gson.fromJson(response, Fixer.class);
    	System.out.println("cotação");
    	System.out.println(fixer.rates.get("BRL"));
    	
    }
}
